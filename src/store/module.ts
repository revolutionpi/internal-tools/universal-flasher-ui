import { StateCreator } from "zustand";

export interface ModuleData {
    is_initialized: boolean;
    size: number;
    type: string;
}

interface ModuleStatus {
    moduleStatus: string;
}

interface Action {
    setStatus: (payload: { status: ModuleStatus['moduleStatus'] }) => void;
    setIsInitialized: (is_initialized: ModuleData["is_initialized"]) => void;
    setSize: (size: ModuleData["size"]) => void;
}

export type ModuleSlice = ModuleData & ModuleStatus & Action;

export const createModuleSlice: StateCreator<ModuleSlice> = (set => ({
    moduleStatus: "UNPLUGGED",
    is_initialized: false,
    size: 0,
    type: "",
    setStatus: ({status}) => set(() => ({ moduleStatus: status })),
    setIsInitialized: (is_initialized) => set(() => ({ is_initialized: is_initialized })),
    setSize: (size) => set(() => ({ size: size })),
}));
