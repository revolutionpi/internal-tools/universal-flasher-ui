import { create } from "zustand";
import { ModuleSlice, createModuleSlice } from "./module";
import { ImageSlice, createImageSlice } from "./image";

export type BoundStore = ModuleSlice & ImageSlice

export const useBoundStore = create<ModuleSlice & ImageSlice>()((...a) => {
    return ({
        ...createModuleSlice(...a),
        ...createImageSlice(...a),
    })
})