import { StateCreator } from "zustand";

export interface ImageSlice {
    image: string | null;
    setImage: (image: string) => void;
}

export const createImageSlice: StateCreator<ImageSlice> = (set) => ({

    image: null,
    setImage: (image) => set(() => ({ image: image })),
})