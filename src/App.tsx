import "@mantine/core/styles.css";
import { Container } from "@mantine/core";
import useSSE from "./hooks/useSSE";
import { useState } from "react";
import { Stepper, Button, Group } from '@mantine/core';
import { InitModule } from "./components/stepper/init-module_step";
import { Progress } from "./components/stepper/flash-module_step";
import { BoundStore, useBoundStore } from "./store/store";
import Verifying from "./components/stepper/verifying-flash_step";
import { useNotification } from "./hooks/useNotification";
import ChooseImage from "./components/stepper/select-image_step";




export default function App() {
    const moduleSetStatus = useBoundStore((state) => state.setStatus)
    const [active, setActive] = useState(0);
    useNotification()
    useSSE("http://10.42.0.164:8000/gpios/module-plug-event", moduleSetStatus)

    const nextStep = () => setActive((current) => (current <= 4 ? (current + 1) % 4 : current));
    const prevStep = () => setActive((current) => (current > 0 ? current - 1 : current));

    return (
        <Container my="md" >

            <Stepper color="kunbusTheme" active={active} onStepClick={setActive} allowNextStepsSelect={false} >

                <Stepper.Step label="First step" description="Choose Linux image">
                    <ChooseImage />
                </Stepper.Step>

                <Stepper.Step label="Second step" description="Init module">
                    <InitModule />
                </Stepper.Step>

                <Stepper.Step label="Third step" description="Flash" >
                    <Progress nextStep={nextStep} prevStep={prevStep} />
                </Stepper.Step>

                <Stepper.Step label="Final step" description="verifying" >
                    <Verifying />
                </Stepper.Step>

                {/* {progress === 100 &&
                        <Stepper.Completed>
                            Completed
                        </Stepper.Completed>
                    } */}

            </Stepper>





            <Group justify="center" mt="xl">
                <Button color="kunbusTheme" variant="light" onClick={prevStep} disabled={active == 3}>Back</Button>
                <Button color="kunbusTheme" onClick={nextStep}
                    disabled={shouldEnableButton(active, useBoundStore())}

                >{active == 1 ? "Flash" : "Next"}</Button>

            </Group>
        </Container>
    );
}

function shouldEnableButton(active: number, store: BoundStore): boolean {
    const stepOne = () => store.image === null
    const stepTwo = () => store.moduleStatus === "UNPLUGGED" || !store.is_initialized
    const stepThree = () => false
    const stepFour = () => false
    const steps = [stepOne, stepTwo, stepThree, stepFour]
    return steps[active]()
}