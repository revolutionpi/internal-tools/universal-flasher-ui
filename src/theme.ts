import { MantineColorsTuple, createTheme } from "@mantine/core";

const kunbusTheme: MantineColorsTuple = [
    '#fff1e2',
    '#ffe2cc',
    '#ffc39b',
    '#ffa264',
    '#fe8637',
    '#fe741a',
    '#ff6c09',
    '#e45a00',
    '#cb4f00',
    '#b14100'
];


export const theme = createTheme({
    colors: {
        kunbusTheme,
    }
});
