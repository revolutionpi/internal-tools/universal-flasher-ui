import { useState } from "react"
import useSSE from "./useSSE"


type Notification = {
    type: string;
    message: string;
}

export default function useNotificationQueue(): Notification | null {
    const [notification, setNotification] = useState<Notification | null>(null)
    useSSE("http://10.42.0.164:8000/notifications", setNotification)
    return notification
}