import { useQuery } from "@tanstack/react-query";
import api from "../api/backend";

export async function cancelFlash() {
    const { data } = await api.delete<boolean>("/module/flash")
    return data;
}

export function useCacnelFlash(cancelProcess: boolean) {
    return useQuery<boolean>({
        queryKey: ["flash", "cancel"], queryFn: cancelFlash, enabled: cancelProcess
    });
}