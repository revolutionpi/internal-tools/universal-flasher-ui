import { QueryFunctionContext, useQuery } from "@tanstack/react-query";
import api from "../api/backend";

export async function postImage(ctx: QueryFunctionContext) {
    const [_, image] = ctx.queryKey;
    const { data } = await api.post("/module/image", { image: image })
    return data;
}

export function usePostImage(image: string | null) {
    return useQuery({ queryKey: ["module", image], queryFn: postImage, enabled: image !== null });
}
