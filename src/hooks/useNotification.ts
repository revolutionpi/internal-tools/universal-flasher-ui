import { useEffect } from "react";
import { notifications } from "@mantine/notifications";
import useNotificationQueue from "./useNotificationQueue";


export function useNotification() {
    const notification  = useNotificationQueue()

    useEffect(() => {
        if (notification)
            notifications.show({
                message: notification?.message,
                title: notification?.type,
                autoClose: false
            })
    }, [notification])
}