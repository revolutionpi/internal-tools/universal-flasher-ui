import { useQuery } from "@tanstack/react-query";
import api from "../api/backend";

export async function getImages() {
    const { data } = await api.get<string[]>("/images")
    return data;
}

export function useGetImages() {
    return useQuery<string[]>({
        queryKey: ["images"], queryFn: getImages,
    });
}