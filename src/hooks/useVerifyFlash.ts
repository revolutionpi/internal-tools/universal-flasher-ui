import { useQuery } from "@tanstack/react-query";
import api from "../api/backend";

export async function verifyFlash() {
    const { data } = await api.get<boolean>("/module/flash/verify")
    return data;
}

export function useVerifyFlash() {
    return useQuery<boolean>({
        queryKey: ["images", "verify"], queryFn: verifyFlash,
        retry: false
    });
}