import { useEffect, useRef } from 'react';


type SSEMessageHandler = (data: any) => void;

// Custom hook to handle Server-Sent Events.
const useSSE = (url: string, onMessage: SSEMessageHandler): EventSource | null => {
    // Use a ref to store the EventSource instance.
    const eventSourceRef = useRef<EventSource | null>(null);

    // useEffect to set up and clean up the EventSource connection.
    useEffect(() => {
        // Create a new EventSource instance with the provided URL.
        eventSourceRef.current = new EventSource(url);

        // Define the onmessage event handler to process incoming messages.
        eventSourceRef.current.onmessage = (event) => {
            const data = JSON.parse(event.data); 
            onMessage(data); // Call the provided message handler with the parsed data.
        };

        // Clean up the EventSource connection when the component unmounts
        return () => {
            eventSourceRef.current?.close(); // Close the EventSource connection if it exists.
        };
    }, [url]);

    // Return the current EventSource instance.
    return eventSourceRef.current;
};

export default useSSE;
