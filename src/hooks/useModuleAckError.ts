import { useQuery } from "@tanstack/react-query";
import api from "../api/backend";

export async function ackError() {
    await api.get("/module/error")
}

export function useModuleAckError(ackErrorFlag: boolean) {
    return useQuery({
        queryKey: ["ackError", ackError], queryFn: ackError, enabled: ackErrorFlag
    });
}