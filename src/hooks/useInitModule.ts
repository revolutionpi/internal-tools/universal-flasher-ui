import { useQuery } from "@tanstack/react-query";
import api from "../api/backend";
import { ModuleData } from "../store/module";

export async function initModule() {
    const { data } = await api.get("/module/init")
    return data;
}


export function useInitModule(isPlugged: string) {
    return useQuery<ModuleData>({
        queryKey: ["module", isPlugged], queryFn: initModule, gcTime: 1000
    });
}