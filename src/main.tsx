import ReactDOM from "react-dom/client";
import App from "./App.tsx";
import { QueryClientProvider } from "@tanstack/react-query";
import queryClient from "./queryClient.tsx";
import { MantineProvider } from "@mantine/core";
import { theme } from "./theme.ts";
import { Notifications } from "@mantine/notifications";
import '@mantine/notifications/styles.css';

ReactDOM.createRoot(document.getElementById("root") as HTMLElement).render(
    <QueryClientProvider client={queryClient}>
        <MantineProvider theme={theme}>
            <Notifications limit={2}   />
            <App />
        </MantineProvider>
    </QueryClientProvider>
);
