import { Flex, List, Loader, LoadingOverlay, Stack, Text, ThemeIcon, rem } from "@mantine/core"
import { useVerifyFlash } from "../../hooks/useVerifyFlash"
import { IconBug, IconCircleCheck } from "@tabler/icons-react"
import Error from "../Error"


interface HttpError {
    response?: {
      data?: any;
    };
  }
  
  const isHttpError = (error: unknown): error is HttpError => {
    return (error as HttpError).response !== undefined;
  };

const Verifying = () => {
    const { isLoading, isFetching, error, isError, isSuccess } = useVerifyFlash()
    const IconComponent = !isError ? IconCircleCheck : IconBug;
    const iconColor = !isError ? "teal" : "red";


    if (isError && isHttpError(error)) {
        return <Error isOpened message={error?.response?.data} />
    }

    return (
        <Flex justify={"center"}>

            <List
                spacing="lg"
                size="lg"
                center
            >
               {isSuccess && <List.Item
                    icon={
                        <ThemeIcon color={iconColor} size={24} radius="xl">
                            <IconComponent style={{ width: rem(16), height: rem(16) }} />
                        </ThemeIcon>
                    }
                >
                    the module has been successfully flashed
                </List.Item>}

            </List>
            <LoadingOverlay visible={isLoading || isFetching} loaderProps={{
                children:
                    <>
                        <Stack justify='center' align='center'>

                            <Loader color='kunbusTheme' type='bars' />
                            <Text > Verifying the module </Text>
                        </Stack>
                    </>
            }} />
        </Flex>
    )
}


export default Verifying