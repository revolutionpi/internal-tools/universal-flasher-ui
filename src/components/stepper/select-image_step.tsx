import { Combobox, Input, InputBase, useCombobox } from "@mantine/core";
import { usePostImage } from "../../hooks/usePostImage";
import { useBoundStore } from "../../store/store";
import { useGetImages } from "../../hooks/useGetImage";

const ChooseImage = () => {
    const selectedImage = useBoundStore(state => state.image)
    const setSelectedImage = useBoundStore(state => state.setImage)
    const { data } = useGetImages()

    usePostImage(selectedImage);


    const combobox = useCombobox({
        onDropdownClose: () => combobox.resetSelectedOption(),
    });

    const options = data?.map((item) => (
        <Combobox.Option value={item} key={item}>
            {item}
        </Combobox.Option>
    ));


    return (
        <>
            <Combobox
                store={combobox}
                onOptionSubmit={(val) => {
                    setSelectedImage(val);
                    combobox.closeDropdown();
                }}
            >
                <Combobox.Target>
                    <InputBase
                        component="button"
                        type="button"
                        pointer
                        rightSection={<Combobox.Chevron />}
                        rightSectionPointerEvents="none"
                        onClick={() => combobox.toggleDropdown()}
                    >
                        {selectedImage || <Input.Placeholder>Pick value</Input.Placeholder>}
                    </InputBase>
                </Combobox.Target>

                <Combobox.Dropdown color="kunbusTheme">
                    <Combobox.Options color="kunbusTheme">{options}</Combobox.Options>
                </Combobox.Dropdown>
            </Combobox>
        </>
    )
}

export default ChooseImage