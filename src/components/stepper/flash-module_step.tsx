import { Button, LoadingOverlay, RingProgress, Stack, Text } from '@mantine/core'
import useSSE from '../../hooks/useSSE';
import { useEffect, useState } from 'react';
import { useCacnelFlash } from '../../hooks/useCancelFlash';
import Error from '../Error';

type ProgressProps = {
    nextStep: Function;
    prevStep: Function;
}

type FlashProgress = { percent: number, is_flashed: boolean, is_canceled: boolean, is_error: boolean, error_message: string }

const initialFlashProgress: FlashProgress = {
    percent: 0,
    is_flashed: false,
    is_canceled: false,
    is_error: false,
    error_message: '',
};

export const Progress: React.FC<ProgressProps> = ({ nextStep, prevStep }) => {
    const [progress, setProgress] = useState<FlashProgress>(initialFlashProgress)
    const [cancel, setCancel] = useState<boolean>(false)
    const evtSrc = useSSE("http://10.42.0.164:8000/module/flash", setProgress)

    const { isLoading, isFetching } = useCacnelFlash(cancel)


    useEffect(() => {
        if (progress.percent === 100 && progress.is_flashed) {
            nextStep(); // Call nextStep when the progress is complete and flashed
        }
        console.log(progress)
        if (progress.is_canceled) {
            prevStep()
            prevStep()
        }
    }, [progress]);

    if (progress.is_error) {
        evtSrc?.close()
        return <Error isOpened message={progress.error_message} buttonOnClick={() => { prevStep(); prevStep() }} />
    }


    return (

        <LoadingOverlay visible={true} loaderProps={{
            children:
                <Stack justify='center' align='center'>
                    <RingProgress
                        // display={active === 2 && progress?.percent !== 100 ? "block" : "none"}
                        sections={[{ value: progress?.percent, color: 'blue' }]}
                        label={
                            <Text c="blue" fw={700} ta="center" size="xl">
                                {progress?.percent}%
                            </Text>
                        }
                    />
                    <Button loading={isLoading || isFetching} onClick={() => setCancel(true)}>
                        Cancel process
                    </Button>
                </Stack>
        }} />
    )
}
