import { Card, Flex, List, Loader, LoadingOverlay, Stack, Text, Image, ThemeIcon, rem, } from '@mantine/core'
import { IconCircleCheck, IconBug } from '@tabler/icons-react'
import { useInitModule } from '../../hooks/useInitModule'
import cm3 from "../../assets/images/CM3.jpg"
import cm4 from "../../assets/images/CM4.png"
import { useBoundStore } from '../../store/store'
import { useEffect } from 'react'


const moduleType = {
    "CM4": cm4,
    "CM3/CM4S": cm3,
}

export const InitModule = () => {
    const moduleStatus = useBoundStore(state => state.moduleStatus)
    const setIsInitialized = useBoundStore(state => state.setIsInitialized)

    const { isLoading, data, isFetching } = useInitModule(moduleStatus)
    useEffect(() => {
        if (data) {
            setIsInitialized(data?.is_initialized)
        }
    }, [data])


    const IconComponent = moduleStatus === "PLUGGED" ? IconCircleCheck : IconBug;
    const iconColor = moduleStatus === "PLUGGED" ? "teal" : "red";


    return (
        <>
            <Flex direction={"column"} justify={"center"} align={"center"} gap={"md"}>

                <List
                    spacing="lg"
                    size="lg"
                    center
                >
                    <List.Item
                        icon={
                            <ThemeIcon color={iconColor} size={24} radius="xl">
                                <IconComponent style={{ width: rem(16), height: rem(16) }} />
                            </ThemeIcon>
                        }
                    >
                        {moduleStatus == "PLUGGED" ? "Module is plugged in" : "Please plug in the module to continue"}
                    </List.Item>

                    {data && <List.Item
                        icon={
                            <ThemeIcon color='teal' size={24} radius="xl">
                                <IconCircleCheck style={{ width: rem(16), height: rem(16) }} />

                            </ThemeIcon>
                        }
                    >
                        Module has been successfully initiliazed
                    </List.Item>}

                </List>

                {data && <Card shadow="sm" w={"auto"} radius="md" withBorder>
                    <Flex align={'center'}>

                        <Card.Section ml="0px">
                            <Image src={moduleType[data.type as keyof typeof moduleType]} h={70} />
                        </Card.Section>

                        <Stack align='flex-start' justify='center' ml="xl"  >
                            <Text >
                                Type: {data.type}
                            </Text>
                            <Text>
                                Size: {data.size}GB
                            </Text>
                        </Stack>
                    </Flex>
                </Card>}

                <LoadingOverlay visible={isLoading || isFetching} loaderProps={{
                    children:
                        <>
                            <Stack justify='center' align='center'>

                                <Loader color='kunbusTheme' type='bars' />
                                <Text > initiliazing the module </Text>
                            </Stack>
                        </>
                }} />
            </Flex>
        </>
    )
}
