import { Alert, Button, Flex, Modal, Text } from "@mantine/core"
import { useDisclosure } from "@mantine/hooks"
import { IconBug } from "@tabler/icons-react"
import React, { useEffect, useState } from "react"
import { useModuleAckError } from "../hooks/useModuleAckError"

interface ErrorProps {
    isOpened: boolean
    message: string
    buttonOnClick?: () => void
}

const Error: React.FC<ErrorProps> = ({ isOpened, message, buttonOnClick = () => { } }) => {
    const [opened, { close }] = useDisclosure(isOpened)
    const [ack, setAck] = useState(false)
    useModuleAckError(ack)
    useEffect(() => {
        if (ack === true) {
            close()
            buttonOnClick()
        }
    }, [ack])
    return (
        <Modal opened={opened} onClose={close} centered withCloseButton={false} >
            <Flex justify="center" align={"center"} direction={"column"} gap={"sm"}>

                <Alert w={"100%"} variant="light" color="red" title="Error" icon={<IconBug />}>
                    <Text size="md">
                        {message}
                    </Text>
                </Alert>
                <Button w={"25%"} onClick={() => setAck(true)} variant="light" >OK</Button>
            </Flex>
        </Modal>
    )
}

export default Error