
# Universal Flasher UI

Universal Flasher UI is a React-based web application designed to streamline the process of flashing modules. It uses modern web development tools and libraries such as Vite, React Query, Mantine, and Zustand.

## Table of Contents

- [Features](#features)
- [Installation](#installation)
- [Usage](#usage)
- [Project Structure](#project-structure)
- [Scripts](#scripts)

## Features

- Multi-step wizard for selecting and flashing Linux images to modules.
- Real-time notifications and server-sent events for status updates and flash progress.

## Installation

### Prerequisites

Ensure you have the following installed:

- [Node.js](https://nodejs.org/) (version 14.x or later)
- [npm](https://www.npmjs.com/)
### Steps

1. Clone the repository:

    ```sh
    git clone https://gitlab.com/revolutionpi/internal-tools/universal-flasher-ui

    cd universal-flasher-ui
    ```

2. Install dependencies:

    ```sh
    npm install
    ```

## Usage

### Development

To start the development server:

```sh
npm run dev
```

This will start the application at `http://localhost:5173/`.

### Build

To build the project for production:

```sh
npm run build
```

The output will be in the `dist` directory.

### Preview

To preview the production build:

```sh
npm run preview
```

## Project Structure

```plaintext
universal-flasher-ui/
└── src/
    ├── api/
    ├── assets/
    ├── components/
    ├── hooks/
    └── store/
```
## Scripts

- `dev`: Starts the development server.
- `build`: Compiles TypeScript and builds the project.
- `lint`: Runs ESLint to check for code quality issues.
- `preview`: Serves the built project for preview.
